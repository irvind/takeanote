package com.irvind.takeanote;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class NoteAdapter extends ArrayAdapter<Note> {

    private Context context;

    public NoteAdapter(Context context, int resourceId) {
        super(context, resourceId);
        this.context = context;
    }

    public NoteAdapter(Context context, int resourceId, List<Note> objects) {
        super(context, resourceId, objects);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Note note = getItem(position);

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.note_list_row, null);
        }

        if (note != null) {
            TextView titleTextView = (TextView)view.findViewById(R.id.node_list_row_title);
            titleTextView.setText(note.getTitle());
        }

        return view;
    }
}
