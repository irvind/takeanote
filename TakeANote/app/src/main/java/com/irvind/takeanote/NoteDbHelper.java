package com.irvind.takeanote;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.irvind.takeanote.Note.NoteTable;

import java.util.ArrayList;
import java.util.List;

public class NoteDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Notes.db";

    private static final String SQL_CREATE_TABLE = "CREATE TABLE " + NoteTable.TABLE_NAME + " (" +
            NoteTable._ID + " INTEGER PRIMARY KEY, " + NoteTable.COLUMN_TITLE + " TEXT, " +
            NoteTable.COLUMN_CONTENT + " TEXT)";
    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + NoteTable.TABLE_NAME;

    public NoteDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }

    public void addNote(Note note) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NoteTable.COLUMN_TITLE, note.getTitle());
        values.put(NoteTable.COLUMN_CONTENT, note.getContent());

        db.insert(NoteTable.TABLE_NAME, null, values);
    }

    public void updateNote(Note note) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NoteTable.COLUMN_TITLE, note.getTitle());
        values.put(NoteTable.COLUMN_CONTENT, note.getContent());

        String selection = NoteTable._ID + " = ?";
        String[] selectionArgs = {String.valueOf(note.getId())};
        int affected = db.update(NoteTable.TABLE_NAME, values, selection, selectionArgs);

        Log.d("AddNoteActivity", String.valueOf(affected));
    }

    public Note getNoteById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        String selectQuery = "SELECT * FROM " + NoteTable.TABLE_NAME + " WHERE " +
                NoteTable._ID + " = " + Integer.toString(id);

        Note note = null;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            note = new Note();
            note.setId(Integer.parseInt(cursor.getString(0)));
            note.setTitle(cursor.getString(1));
            note.setContent(cursor.getString(2));
        }

        return note;
    }

    public List<Note> getAllNotes() {
        SQLiteDatabase db = getReadableDatabase();
        String selectQuery = "SELECT * FROM " + NoteTable.TABLE_NAME;
        List<Note> notes = new ArrayList<Note>();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(Integer.parseInt(cursor.getString(0)));
                note.setTitle(cursor.getString(1));
                note.setContent(cursor.getString(2));
                notes.add(note);
            } while (cursor.moveToNext());
        }

        return notes;
    }

    public void deleteAllNotes() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(NoteTable.TABLE_NAME, null, null);
    }
}
