package com.irvind.takeanote;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class AddNoteActivity extends ActionBarActivity {

    private NoteDbHelper noteDbHelper;
    private boolean newNoteFlag;

    private EditText titleEditText;
    private EditText contentEditText;

    private int editId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        titleEditText = (EditText)findViewById(R.id.title_edit_text);
        contentEditText = (EditText)findViewById(R.id.content_edit_text);

        noteDbHelper = new NoteDbHelper(this);

        Intent intent = getIntent();
        newNoteFlag = intent.getBooleanExtra(NoteListActivity.NEW_NOTE_FLAG, true);
        if (!newNoteFlag) {
            Note note = noteDbHelper.getNoteById(intent.getIntExtra(NoteListActivity.NOTE_ID, 0));

            editId = note.getId();
            titleEditText.setText(note.getTitle());
            contentEditText.setText(note.getContent());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_save_note:
                saveNote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveNote() {
        Note note = new Note();
        note.setId(editId);
        note.setTitle(titleEditText.getText().toString());
        note.setContent(contentEditText.getText().toString());

        if (newNoteFlag) {
            noteDbHelper.addNote(note);
        } else {
            noteDbHelper.updateNote(note);
        }

        finish();
    }
}
