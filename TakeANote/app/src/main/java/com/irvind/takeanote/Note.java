package com.irvind.takeanote;

import android.provider.BaseColumns;

public final class Note {

    private int id;
    private String title;
    private String content;

    public Note() {}

    public Note(int _id, String _title, String _content) {
        id = _id;
        title = _title;
        content = _content;
    }

    public Note(String _title, String _content) {
        title = _title;
        content = _content;
    }

    public static abstract class NoteTable implements BaseColumns {
        public static final String TABLE_NAME = "notes";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_CONTENT = "content";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
