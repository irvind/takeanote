package com.irvind.takeanote;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class NoteListActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    public final static String NEW_NOTE_FLAG = "com.irvind.takeanote.NEW_NOTE_FLAG";
    public final static String NOTE_ID = "com.irvind.takeanote.NOTE_ID";

    private NoteDbHelper noteDbHelper;
    private NoteAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);

        noteDbHelper = new NoteDbHelper(this);

        ListView noteListView = (ListView)findViewById(R.id.note_list_view);
        adapter = new NoteAdapter(this, R.layout.note_list_row);
        noteListView.setAdapter(adapter);
        noteListView.setOnItemClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        adapter.clear();

        List<Note> notes = noteDbHelper.getAllNotes();
        adapter.addAll(notes);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.note_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_add_note:
                startAddNoteActivity();
                return true;
            case R.id.action_clear_notes:
                clearNotes();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Note clickNote = (Note)parent.getItemAtPosition(position);
        startEditNoteActivity(clickNote.getId());

//        Toast toast = Toast.makeText(getApplicationContext(), clickNote.getTitle(), Toast.LENGTH_SHORT);
//        toast.show();
    }

    private void clearNotes() {
        noteDbHelper.deleteAllNotes();
        adapter.clear();
    }

    private void startAddNoteActivity() {
        Intent intent = new Intent(this, AddNoteActivity.class);
        intent.putExtra(NEW_NOTE_FLAG, true);

        startActivityForResult(intent, 0);
    }

    private void startEditNoteActivity(int noteId) {
        Intent intent = new Intent(this, AddNoteActivity.class);
        intent.putExtra(NEW_NOTE_FLAG, false);
        intent.putExtra(NOTE_ID, noteId);

        startActivityForResult(intent, 0);
    }
}
